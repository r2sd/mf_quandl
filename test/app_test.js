/* global it */
const assert = require('assert')
const appendApiKey = require('../app').appendApiKey
const fetchCsv = require('../app').fetchCsv
const downloadListOfFunds = require('../app').downloadListOfFunds
const processPipeAsCsv = require('../app').processPipeAsCsv
const fs = require('fs')
require('dotenv').config()

it('assert if api key is appended', () => {
  let qs = {}
  appendApiKey(qs)
  assert.deepStrictEqual(
    qs,
    {
      'api_key': process.env.QUANDL_API_KEY
    }
  )
})

it('test processPipeAsCsv', async () => {
  processPipeAsCsv(
    fs.createReadStream('./test/AMFI_metadata.csv'),
    (result) => assert.deepStrictEqual(result, [
      {
        code: '100033',
        name: 'Aditya Birla Sun Life Mutual Fund - Aditya Birla Sun Life Equity Advantage Fund - Regular Growth',
        description: 'ISIN Div Payout/ ISIN Growth: INF209K01165',
        refreshed_at: '2018-09-05 20:01:18',
        from_date: '2016-08-30',
        to_date: '2018-09-05'
      },
      {
        code: '100034',
        name: 'Aditya Birla Sun Life Mutual Fund - Aditya Birla Sun Life Equity Advantage Fund -Regular Dividend',
        description: 'ISIN Div Payout/ ISIN Growth: INF209K01157<br><br>ISIN Div Reinvestment: INF209K01CE5',
        refreshed_at: '2018-09-05 20:01:18',
        from_date: '2016-08-30',
        to_date: '2018-09-05'
      }
    ])
  )
})

it('test fetchCsv', async () => {
  const queryString = {
    'start_date': '2018-09-03',
    'end_date': '2018-09-03'
  }
  await fetchCsv(
    'URI',
    queryString,
    () => fs.createReadStream('./test/AMFI_metadata.csv')
  )
  assert.strictEqual(queryString.format, 'csv')
})

it('assert list from zip', async () => {
  assert.deepStrictEqual(
    await downloadListOfFunds(() => fs.createReadStream('./test/pass.zip')),
    [{ a: 'd', b: 'e', c: 'f' }, { a: 'g', b: 'h', c: 'i' }]
  )
})

it('handle error from zip when csv is not found', async () => {
  await assert.rejects(downloadListOfFunds(() => fs.createReadStream('./test/fail1.zip')))
})

it('handle error from zip when csv is invalid', async () => {
  await assert.rejects(downloadListOfFunds(() => fs.createReadStream('./test/fail2.zip')))
})
