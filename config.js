require('dotenv').config()

module.exports = {
  'quandl_api_key': process.env.QUANDL_API_KEY
}
