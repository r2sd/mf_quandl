const config = require('./config')
const request = require('request')
const unzip = require('unzip')
const csvParse = require('csv-parse')
const db = require('./models')
const Sequelize = require('sequelize')
const moment = require('moment-timezone')
const pLimit = require('p-limit')
const fs = require('fs')

const metadataUrl = 'https://www.quandl.com/api/v3/databases/AMFI/metadata'

const startDate = moment.utc(process.env.START_DATE, 'YYYY-MM-DD')

const appendApiKey = (requestQueryString) => {
  requestQueryString.api_key = config.quandl_api_key
}

const processPipeAsCsv = (inputPipe, resolve, reject) => {
  const parser = csvParse({
    columns: true
  })
  let output = []
  inputPipe
    .pipe(parser)
    .on('error', (err) => reject(err))
    .on('data', (data) => output.push(data))
    .on('end', () => resolve(output))
}

const fetchCsv = async (uri, queryString = {}, reqLib = request) => {
  const options = {
    uri,
    qs: queryString
  }
  appendApiKey(options.qs)
  options.qs.format = 'csv'
  return new Promise((resolve, reject) => {
    processPipeAsCsv(
      reqLib(options).on('error', (err) => reject(err)),
      resolve,
      reject
    )
  })
}

const downloadListOfFunds = async (reqLib = request, unzipLib = unzip) => {
  const options = {
    uri: metadataUrl,
    qs: {
      'encoding': null
    }
  }
  appendApiKey(options.qs)
  return new Promise((resolve, reject) => {
    reqLib(options)
      .on('error', (err) => reject(err))
      .pipe(unzipLib.Parse())
      .on('error', (err) => reject(err))
      .on('entry', entry => {
        const fileName = entry.path
        const type = entry.type
        if (fileName === 'AMFI_metadata.csv') {
          console.log('Successfully downloaded and extracted zip file containing "AMFI_metadata.csv"')
          processPipeAsCsv(entry, resolve, reject, ['Date', 'Net Asset Value', 'Repurchase Price', 'Sale Price'])
        } else {
          entry.autodrain()
          console.log(`Got unexpected "${type}" named "${fileName}" in the zip file`)
          reject(new Error('Something went wrong!'))
        }
      })
  })
}

const updateFundsList = async () => {
  const fundsList = await downloadListOfFunds()
  for (let i = 0, len = fundsList.length; i < len; i++) {
    if ((i > 0) && ((i + 1) % 500 === 0 || i === len - 1)) {
      console.log(`${i + 1}/${len} (${Math.round((i + 1) / len * 100)}%)`)
    }
    const element = fundsList[i]
    await db.Metadata.upsert({
      code: element.code,
      name: element.name,
      description: element.description,
      refreshed_at: moment.utc(element.refreshed_at, 'YYYY-MM-DD hh:mm:ss').toDate(),
      from_date: moment.utc(element.from_date, 'YYYY-MM-DD').toDate(),
      to_date: moment.utc(element.to_date, 'YYYY-MM-DD').toDate()
    })
  }
}

const getFundsList = async (atLeastActiveInLastXDays = 30) => {
  return db.Metadata.findAll({
    where: {
      id: {
        [Sequelize.Op.in]: [9025, 4812, 4813, 9743, 9648, 10234, 10235, 5980, 5981]
      },
      to_date: {
        [Sequelize.Op.gte]: moment().subtract(atLeastActiveInLastXDays, 'days').toDate()
      },
      name: {
        [Sequelize.Op.iLike]: '%direct%'
      }
    },
    order: ['id']
  })
}

const getFundNavDetails = async metadataObject => {
  console.log(`Downloading ${metadataObject.id}`)
  const latestNav = await db.NAVDetails.findOne({
    where: { metadata_id: metadataObject.id },
    order: [['date', 'DESC']]
  })
  let fromDate = startDate
  if (latestNav) {
    fromDate = moment(latestNav.date).add(1, 'days')
  } else {
    fs.appendFile('./no_existing_data.txt', `${metadataObject.id}\n`, err => {
      if (err) {
        console.log(err)
      }
    })
  }
  const fundNavDetails = await fetchCsv(
    `https://www.quandl.com/api/v3/datasets/AMFI/${metadataObject.code}`,
    {
      start_date: fromDate.format('YYYY-MM-DD')
    }
  )
  for (let i = 0, len = fundNavDetails.length; i < len; i++) {
    const record = fundNavDetails[i]
    await db.NAVDetails.create({
      metadata_id: metadataObject.id,
      date: moment.utc(record.Date, 'YYYY-MM-DD'),
      net_asset_value: record['Net Asset Value']
    })
  }
  if (!latestNav && fundNavDetails.length === 0) {
    fs.appendFile('./no_future_data.txt', `${metadataObject.id}\n`, err => {
      if (err) {
        console.log(err)
      }
    })
  }
}

const main = async () => {
  try {
    await db.setupDatabase()
    // await updateFundsList()
    const listOfFunds = await getFundsList()
    const limit = pLimit(parseInt(process.env.MAX_CONCURRENT_DOWNLOADS))
    await Promise.all(listOfFunds.map(
      fundMetadata => limit(() => getFundNavDetails(fundMetadata))
    ))
  } catch (e) {
    console.log(e)
  }
}

main()

module.exports.appendApiKey = appendApiKey
module.exports.processPipeAsCsv = processPipeAsCsv
module.exports.fetchCsv = fetchCsv
module.exports.downloadListOfFunds = downloadListOfFunds
