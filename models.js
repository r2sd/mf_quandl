const Sequelize = require('sequelize')
require('dotenv').config()

const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASS,
  {
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT,
    operatorsAliases: false,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    logging: false
  }
)

const Metadata = sequelize.define(
  'metadata',
  {
    code: { type: Sequelize.INTEGER, allowNull: false },
    name: { type: Sequelize.TEXT, allowNull: false },
    description: { type: Sequelize.TEXT, allowNull: false },
    refreshed_at: { type: Sequelize.DATE, allowNull: false },
    from_date: { type: Sequelize.DATEONLY, allowNull: false },
    to_date: { type: Sequelize.DATEONLY, allowNull: false }
  },
  {
    freezeTableName: true,
    timestamps: false,
    indexes: [
      { fields: ['code'], unique: true },
      { fields: ['to_date'] },
      { fields: ['refreshed_at'] }
    ]
  }
)

const NAVDetails = sequelize.define(
  'nav_details',
  {
    metadata_id: {
      allowNull: false,
      type: Sequelize.INTEGER,
      references: {
        model: Metadata,
        key: 'id'
      }
    },
    date: { type: Sequelize.DATEONLY, allowNull: false },
    net_asset_value: { type: Sequelize.DECIMAL(10, 4), allowNull: false }
  },
  {
    freezeTableName: true,
    timestamps: false,
    indexes: [
      { fields: ['metadata_id', 'date'], unique: true }
    ]
  }
)

const setupDatabase = async (reset = false) => {
  let options
  if (reset) {
    options = { force: true }
  }
  await sequelize.sync(options)
}

module.exports = {
  setupDatabase,
  Metadata,
  NAVDetails
}
